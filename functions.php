<?php
/**
 * @package Taplog
 * This Template For Displaying All Function
 * This Theme for BLog Site.
 */

require_once get_template_directory()."/inc/enqueue.php";
require_once get_template_directory()."/inc/theme_support.php";
require_once get_template_directory()."/inc/extra.php";
require_once get_template_directory()."/inc/widget.php";
require_once get_template_directory()."/inc/class-wp-bootstrap-navwalker.php";
require_once get_template_directory()."/inc/wp-comment-walker.php";
