<?php
/**
 * @package Taplog
 * This Template For Displaying Page.
 * This Theme for BLog Site.
 */
get_header();
?>



<div class="not-found element-animate">
    <div class="content">
        <h1><span class="first">4</span><i aria-hidden="true">&#9785;</i><span class="second">4</span></h1>
        <h3>ERROR</h3>
    </div>
    <p>The Page You Looking for Can't Be Found Go Home By <a href="<?php echo esc_url(home_url())?>">Clicking Here</a></p>
</div>





<?php get_footer()?>