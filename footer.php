<?php wp_footer()?>
<footer class="site-footer" role="contentinfo">
        <div class="container">
          <div class="row mb-5">

              <div class="col-md-3">
                  <h3>Quick Links</h3>
                  <ul class="list-unstyled">
                      <li><a href="#">About Us</a></li>
                      <li><a href="#">Travel</a></li>
                      <li><a href="#">Adventure</a></li>
                      <li><a href="#">Courses</a></li>
                      <li><a href="#">Categories</a></li>
                  </ul>
              </div>

                <div class="col-md-6">
                  <h3>Latest Post</h3>
                  <div class="post-entry-sidebar">
                    <ul>
                        <?php
                        $rand = new WP_Query(array(
                            'post_type'      => 'post',
                            'post_status'    => 'publish',
                            'orderby'        => 'rand',
                            'posts_per_page'  => 3
                        ));
                        if($rand->have_posts()): while($rand->have_posts()): $rand->the_post();
                        ?>
                      <li>
                        <div>
                          <img src="<?php echo esc_url(get_the_post_thumbnail_url());?>" alt="Image placeholder" class="mr-4">
                          <div class="text">
                            <h4><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
                            <div class="post-meta">
                                <ul class="post-meta-data">
                                    <li class="mr-2">
                                        <?php the_time('M j,Y')?>
                                    </li> &bullet;
                                    <li class="ml-2">
                                        <div class="com"><img src="<?php echo get_theme_file_uri('images/chat.svg')?>" alt="commenticon">
                                            <?php comments_number('0','one','%')?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                          </div>
                        </div>
                      </li>
                      <?php endwhile; wp_reset_postdata(); endif;?>
                    </ul>
                  </div>
                </div>

<!--                  This is for social media link-->
                  <div class="col-md-3">
                    <h3>Social</h3>
                    <ul class="list-unstyled footer-social">
                      <li><a href="<?php esc_url('https://plus.google.com/u/0/114394605818027719962')?>" target="_blank">Goolge Plus</a></li>
                      <li><a href="<?php esc_url('https://www.facebook.com/ittech360')?>" target="_blank"> Facebook</a></li>
                      <li><a href="<?php esc_url('https://www.youtube.com/channel/UCqmk5A3JziCIpO7a7EBNUkA/featured')?>" target="_blank"> YouTube 1</a></li>
                      <li><a href="<?php esc_url('https://www.youtube.com/channel/UCzJaVjwqaUQzXCoyoYTZAkA')?>" target="_blank"></span> YouTube 2</a></li>
                    </ul>
                  </div>


          </div>
          <div class="row">
            <div class="col-md-12 text-center">
              <p class="small">
            Copyright &copy; All Rights Reserved | This template is made with <i class="text-danger" aria-hidden="true">&#9829;</i> by
                  <a href="<?php echo esc_url('https://facebook.com/anik.ishaan')?>" target="_blank">Anik Ishaan</a>
            </p>
            </div>
          </div>
        </div>
      </footer>
      <!-- END footer -->

    </div>
    
    <!-- loader -->
<!--    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>-->

  
  </body>
</html>