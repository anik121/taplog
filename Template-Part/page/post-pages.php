<?php
/**
 * @package Taplog
 * This Template For Displaying Single Pages Contact
 * This Theme for BLog Site.
 */
?>

<?php if(have_posts()): the_post();?>

    <h1 class="mb-4 title"><?php the_title()?></h1>

    <div class="post-content-body">
        <?php the_content()?>
    </div>

<?php endif;?>
