<?php
/**
 * @package Taplog
 * This Template For Displaying 2 Column Post Style
 * This Theme for BLog Site.
 */
?>

<?php if(have_posts()): while(have_posts()): the_post();?>
<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-6'); ?>>
    <a href="<?php the_permalink()?>" class="blog-entry element-animate" data-animate-effect="fadeIn">
        <?php if(has_post_thumbnail()):?>
        <div class="thumbnail">
            <?php the_post_thumbnail()?>
        </div>
        <?php endif;?>
        <div class="blog-content-body">
            <div class="post-meta">
                <ul class="post-meta-data">
                    <li class="author mr-2">
                        <?php echo get_avatar(get_the_author_meta('ID'),32);?>
                        <?php the_author()?>
                    </li>&bullet;
                    <li class="mr-2">
                        <?php the_time('M j,Y')?>
                    </li> &bullet;
                    <li class="ml-2">
                    <div class="com">
                    <?php if (function_exists('wpp_get_views')):?>
                    <img src="<?php echo get_theme_file_uri('images/eye.svg')?>" alt="viewing">
                    <?php echo wpp_get_views(get_the_ID()); endif;?>
                    </div>
                    </li>
                </ul>
            </div>
            <h2>
                <?php the_title()?>
            </h2>
        </div>
    </a>
</div>
<?php endwhile; endif;?>