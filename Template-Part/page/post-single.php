<?php
/**
 * @package Taplog
 * This Template For Displaying Single Post Content
 * This Theme for BLog Site.
 */
?>

<?php if(have_posts()): the_post();?>

    <?php if(has_post_thumbnail()):?>
        <div class="single-thumbnail">
            <?php the_post_thumbnail()?>
        </div>
    <?php endif;?>
    <div class="post-meta">
    <ul class="post-meta-data">
                    <li class="author mr-2">
                        <?php echo get_avatar(get_the_author_meta('ID'),32);?>
                        <?php the_author()?>
                    </li>&bullet;
                    <li class="mr-2">
                        <?php the_time('M j,Y')?>
                    </li> &bullet;
                    <li class="ml-2">
                    <div class="com">
                    <?php if (function_exists('wpp_get_views')):?>
                    <img src="<?php echo get_theme_file_uri('images/eye.svg')?>" alt="viewing">
                    <?php echo wpp_get_views(get_the_ID()); endif;?>
                    </div>
                    </li>
                </ul>
    </div>
    <h1 class="mb-4"><?php the_title()?></h1>

    <p class="cat mb-5"><?php the_category(' &bull; ')?></p>

    <div class="post-content-body">
        <?php the_content()?>
    </div>

<?php endif;?>
