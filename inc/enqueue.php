<?php
/*
 @package Taplog

This Template For Enqueue All Css And Javascript File
 This Theme for BLog Site.
*/
if(!function_exists('include_css_and_js')):
function include_css_and_js(){
    wp_enqueue_style( 'stylesheet', get_stylesheet_uri());
    wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css', array(), null, 'all' );
    wp_enqueue_style( 'animation', get_theme_file_uri('css/animate.css'), array(), null, 'all' );
    wp_enqueue_style( 'owl', get_theme_file_uri('css/owl.carousel.min.css'), array(), null, 'all' );
    // include javascript
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js', array(), null, true);
    wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js', array(), null, true);
    wp_enqueue_script( 'waypoints', get_theme_file_uri('js/jquery.waypoints.min.js'), array(), null, true);
    wp_enqueue_script( 'stellar', get_theme_file_uri('js/jquery.stellar.min.js'), array(), null, true);
    wp_enqueue_script( 'main', get_theme_file_uri('js/main.js'), array(), null, true);

    // Load comment-reply.js (into footer)
    if( !function_exists('enqueue_script_func')):
        function enqueue_script_func(){
            if( is_singular() && comments_open() && ( get_option( 'thread_comments' ) == 1) ) {

                wp_enqueue_script( 'comment-reply', 'wp-includes/js/comment-reply', array(), false, true );
            }
        }
    endif;
}
endif;
add_action( 'wp_enqueue_scripts', 'include_css_and_js');
