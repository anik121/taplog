<?php
class Bootstrap_Comment_Walker extends Walker_Comment {
    protected function html5_comment( $comment, $depth, $args ) {

?>

    <li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>

    <?php if ( 0 != $args['avatar_size'] ): ?>
    <div class="vcard">
        <?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
    </div>
    <?php endif; ?>

    <div class="comment-body">
    <?php printf( '<h3 class="user-name">%s</h3>', get_comment_author_link() ); ?>
    <div class="meta">
            <time datetime="<?php comment_time( 'c' ); ?>">
                <?php printf( _x( '%1$s at %2$s', '1: date, 2: time','taplog' ), get_comment_date(), get_comment_time() ); ?>
            </time>
    </div><!-- .comment-metadata -->

    <?php if ( '0' == $comment->comment_approved ) : ?>
    <p class="comment-awaiting-moderation label label-info"><?php _e( 'Your comment is awaiting moderation.','taplog' ); ?></p>
    <?php endif; ?>             

    <div class="comment-content">
         <?php comment_text(); ?>
    </div>

    <ul class="list-inline">
        <?php edit_comment_link( __( 'Edit','taplog' ), '<li class="edit-link">', '</li>' ); ?>

    <?php
        comment_reply_link( array_merge( $args, array(
            'add_below' => 'div-comment',
            'depth'     => $depth,
            'max_depth' => $args['max_depth'],
            'before'    => '<li class="reply-link">',
            'after'     => '</li>'
        ) ) );  
    ?>
    </ul>
    </div>

<?php
    }   
}?>