<?php
/**
 * @package Taplog
 * This Template For Displaying home Page.
 * This Theme for BLog Site.
 */
if ( ! isset( $content_width ) ) {
    $content_width = 640;
}
/*================================
      After Theme Setup Functions
==================================*/
if(!function_exists('after_theme_setup_func_on_taplog')):

function after_theme_setup_func_on_taplog(){
    register_nav_menus(array(
        'Primary Menu' => __('Header Navigation Menu Bar','taplog'),
        'Terms Menu' => __('Terms Menu Bar','taplog'),
    ));
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
    add_theme_support('automatic-feed-links');
    add_theme_support('customize-selective-refresh-widgets');
    add_editor_style();
    add_theme_support( 'custom-background', apply_filters( 'after_theme_setup_func', array(
        'default-color' => 'fff',
        'default-image' => '',
    ) ) );
    add_theme_support('custom-logo',array(
        'width'      => 300,
        'height'     => 100,
        'flex-width' => true,
        'flex-height'=> true,
        'header-text'=> array('Website Title','Website Description')
    ));
    load_theme_textdomain('taplog');
}
else:
    echo "<h1>SomeThis Went Wrong";

endif;

add_action('after_setup_theme','after_theme_setup_func_on_taplog');

/*=================================
 * Added Breadcrumb On This Theme
 * ===================================
 * */
function get_breadcrumb() {
    echo '<a href="'.esc_url(home_url()).'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
        if (is_single()) {
            echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
            the_title();
        }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo the_search_query();
    }elseif (is_tag()){
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo single_tag_title();
    }elseif (is_archive()){
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_archive_title();
    }
}
