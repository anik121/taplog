<?php
/**
 * @package Taplog
 * This Template For Displaying Header Section
 * This Theme for BLog Site.
 */
?>

<!DOCTYPE html>
<html <?php language_attributes()?>>

<head>
    <meta charset="<?php bloginfo('charset')?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head()?>
</head>

<body <?php body_class()?>>

    <div class="wrap">

        <header role="banner">
            <div class="top-bar">
                <div class="cc">
                    <div class="tb">
                        <div class="term">
                            <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'Terms Menu',
                        'depth'             => 1,
                    ));
                    ?>
                        </div>
                        <a href="javascript:void(0)" class="search-trigger"><img src="<?php echo get_theme_file_uri('images/loupe.svg')?>"
                                alt="searchicon"></i></a>
                    </div>
                </div>
            </div>

            <div class="search-top">
                <script>
                    (function () {
                        var cx = '016391769434029146189:tmlrvfg4baw';
                        var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                        gcse.async = true;
                        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(gcse, s);
                    })();
                </script>
                <gcse:search></gcse:search>
            </div>

            <div class="container logo-wrap">
                <div class="row pt-5">
                    <div class="col-12 text-center">
                        <a class="absolute-toggle d-block d-md-none" data-toggle="collapse" href="#navbarMenu" role="button"
                            aria-expanded="false" aria-controls="navbarMenu"><span class="burger-lines"></span></a>
                        <h1 class="site-logo">
                            <?php if(has_custom_logo()):
                           echo get_custom_logo();
                        else:?>
                            <a href="<?php echo esc_url(home_url());?>">
                                <?php bloginfo('name');?></a>
                            <?php endif;?>
                        </h1>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-expand-md  navbar-light bg-light" role="navigation">
                <div class="container">
                    <?php
                wp_nav_menu( array(
                    'theme_location'    => 'Primary Menu',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'navbarMenu',
                    'menu_class'        => 'navbar-nav mx-auto st',
                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'            => new WP_Bootstrap_Navwalker(),
                ) );
                ?>
                </div>
            </nav>
        </header>
        <!-- END header -->

        <a href="javascript:void(0)" class="tap-trigger"><img src="<?php echo get_theme_file_uri('images/arrow.svg')?>"
                alt="arowtop"></a>