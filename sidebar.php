<?php
/**
 * @package Taplog
 * This Template For Displaying Sidebar Page.
 * This Theme for BLog Site.
 */
?>



    <div class="sidebar-box">
        <div class="bio text-center">
            <div class="img-fluid">
                <?php echo get_avatar(get_the_author_meta('ID'),96);?>
            </div>
            <div class="bio-body">
                <h2><?php echo get_the_author_meta('display_name'); ?></h2>
                <p><?php echo get_the_author_meta('description')?></p>
                <p><a href="#" class="btn btn-outline-dark btn-sm rounded">Read my bio</a></p>
                <p class="social">
                    <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
                    <a href="#" class="p-2"><span class="fa fa-twitter"></span></a>
                    <a href="#" class="p-2"><span class="fa fa-instagram"></span></a>
                    <a href="#" class="p-2"><span class="fa fa-youtube-play"></span></a>
                </p>
            </div>
        </div>
    </div>

    <!-- END Sidebar-author-Bio-Box -->

    <div class="sidebar-box">
        <h3 class="heading">Popular Posts</h3>

        <div class="post-entry-sidebar">
            <ul>
                <?php
                $popular_posts_args = array(
                    'posts_per_page' => 3,
                    'meta_key' => 'my_post_viewed',
                    'orderby' => 'meta_value_num',
                    'order'=> 'DESC'
                   );
             
                   $popular_posts_loop = new WP_Query( $popular_posts_args );

                if($popular_posts_loop-> have_posts()): while ($popular_posts_loop -> have_posts()): $popular_posts_loop-> the_post(); ?>
                <li>
                    <div>
                        <?php if(has_post_thumbnail()):?>
                        <div class="mr-4">
                            <?php the_post_thumbnail();?>
                        </div>
                        <?php endif;?>
                        <div class="text t">
                            <h4><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
                            <div class="post-meta">
                                <span class="mr-2"> <?php the_time('F j, Y')?></span>
                            </div>
                        </div>
                    </div>
                </li>

                <?php
                endwhile;
                wp_reset_postdata();
                endif;
                ?>
            </ul>
        </div>
    </div>

    <!-- END Sidebar-Popular-Post-Box -->

    <div class="sidebar-box">
        <ul class="categories">
            <?php wp_list_categories(array(
                'order' => 'ASC',
                'show_count' => true,
                'pad_counts' => true,
                'title_li' => '<h3 class="heading">'. __( 'Category', 'taplog' ).'</h3>',
            ));?>
        </ul>
    </div>
    <!-- END sidebar-box -->
    <?php
    if(is_active_sidebar('sidebar')):
        dynamic_sidebar('sidebar');
    endif;
    ?>

<!-- END sidebar -->
