<?php
/**
 * @package Taplog
 * This Template For Displaying home Page.
 * This Theme for BLog Site.
 */
get_header();
?>
<section class="site-section py-lg">
    <div class="container">

        <div class="row blog-entries element-animate">
            <div class="col-md-12 col-lg-8 main-content">

                <?php get_template_part('Template-Part/page/post','single')?>

                <!-- this is post tag -->
                <div class="pt-5 cat-tag">
                    <p>
                        <?php if(has_tag()):?>
                        <span class="for_tag">Tags:
                            <?php the_tags( ' ', ' ', ' ' );?>
                        </span>
                        <?php endif;?>
                    </p>
                </div>

                <?php if ( is_singular( 'attachment' ) ) {
                // Parent post navigation.
                the_post_navigation(
                array(
                /* translators: %s: parent post link */
                'prev_text' => sprintf( __( '<span class="meta-nav">Published in</span><span class="post-title">%s</span>', 'taplog' ), '%title' ),
                )
                );
                } elseif ( is_singular( 'post' ) ) {
                // Previous/next post navigation.
                the_post_navigation(
                array(
                'next_text' => '<span class=" btn btn-outline-dark btn-sm rounded" aria-hidden="true">' . __( 'Next Post', 'taplog' ) . '</span> '.
                '<p class="post-title">%title</p>',
                'prev_text' => '<span class="btn btn-outline-dark btn-sm rounded" aria-hidden="true">' . __( 'Previous Post', 'taplog' ) . '</span> '.
                '<p class="post-title">%title</p>',
                )
                );
                }
                ?>

            

                <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
                ?>
            </div>
            <?php ?>
            <!-- Start Sidebar Contact-->
            <div class="col-md-12 col-lg-4 sidebar">
                <?php get_sidebar()?>
            </div>
        </div>

    </div>
</section>


<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-3 title">Related Post</h2>
            </div>
        </div>

        <?php
  $orig_post = $post;
  global $post;
  $tags = wp_get_post_tags($post->ID);
   
  if ($tags) {
  $tag_ids = array();
  foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
  $args=array(
  'tag__in' => $tag_ids,
  'post__not_in' => array($post->ID),
  'posts_per_page'=>4, 
  'ignore_sticky_posts'=>1
  );
   
  $my_query = new wp_query( $args );
 
  ?>

        <div class="row">
            <?php 

while( $my_query->have_posts() ) { $my_query->the_post();
?>
            <div class="col-md-6 col-lg-3">
                <div class="a-block sm d-flex align-items-center height-md" style="background-image: url('<?php echo esc_url(get_the_post_thumbnail_url())?>'); ">
                    <div class="text">
                        <div class="post-meta">
                            <ul class="post-meta-data">
                                <li class="author mr-2">
                                    <?php echo get_avatar(get_the_author_meta('ID'),32);?>
                                    <?php the_author()?>
                                </li>&bullet;
                                <li class="mr-2">
                                    <?php the_time('M j,Y')?>
                                </li> &bullet;
                                <li class="ml-2">
                                    <div class="com"><img src="<?php echo get_theme_file_uri('images/chat.svg')?>" alt="commenticon">
                                        <?php comments_number('0','one','%')?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <h3>
                            <a href="<?php the_permalink()?>"><?php the_title()?></a>
                        </h3>
                    </div>
                </div>
            </div>


            <?php }
  }
  $post = $orig_post;
  wp_reset_query();
  ?>
        </div>
    </div>


</section>
<!-- END section -->

<?php get_footer()?>