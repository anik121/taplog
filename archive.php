<?php
/**
 * @package Taplog
 * This Template For Displaying Archive Page.
 * This Theme for BLog Site.
 */
get_header();
?>

    <!--this section for slider-->
    <section class="site-section pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme home-slider">
                       <?php get_template_part( 'Template-Part/page/carousel')?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--The End Of Slider-->

    <section class="site-section py-sm">
        <div class="container">
            <div class="row">
                    <div class="col-md-6">
                        <h2 class="mb-4 breadcrumbs"><?php get_breadcrumb()?></h2>
                    </div>
            </div>

            <div class="row blog-entries">
                <div class="col-md-12 col-lg-8 main-content">

                        <?php get_template_part('Template-Part/page/post','list')?>

                    <?php $paginatinos = array(
                        'prev_text'          => __('Previews','taplog'),
                        'next_text'          => __('Next','taplog'),
                    );
                    ?>
                    <div class="row mt-5">
                        <div class="col-md-12 text-center">
                            <div id="page-pagination">
                                <?php echo paginate_links($paginatinos); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-4 sidebar">
                    <?php get_sidebar()?>
                </div>

            </div>
        </div>
    </section>


<?php get_footer( )?>