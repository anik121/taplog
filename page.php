<?php
/**
 * @package Taplog
 * This Template For Displaying Page.
 * This Theme for BLog Site.
 */
get_header();
?>


<section class="site-section py-lg">
    <div class="container">
        <div class="row blog-entries element-animate">
            <div class="col-md-12 col-lg-8 main-content">

                <?php get_template_part('Template-Part/page/post','pages')?>

    <div class="wp-page-link"> <?php wp_link_pages();?> </div>

                <div class="row mb-5 mt-5">
                    <div class="col-md-12 mb-5">
                        <h2 class="title">My Latest Posts</h2>
                    </div>

                    <div class="col-md-12">
                        <?php get_template_part('Template-Part/page/post','rand')?>
                    </div>
                </div>

            </div>

            <div class="col-md-12 col-lg-4 sidebar">
                <?php get_sidebar()?>
            </div>
        </div>
    </div>
</section>

<?php get_footer()?>